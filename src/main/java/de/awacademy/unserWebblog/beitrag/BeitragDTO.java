package de.awacademy.unserWebblog.beitrag;

import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class BeitragDTO {

    //Überschrift auch @NotEmpty

    @NotEmpty
    private String text = "";

    @NotEmpty
    private String ueberschrift = "";
    private Instant erstellungsDatum;

    public String getText() {
        return text;
    }


    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public Instant getErstellungsDatum() {
        return erstellungsDatum;
    }

    public void setErstellungsDatum(Instant erstellungsDatum) {
        this.erstellungsDatum = erstellungsDatum;
    }

    public void setText(String text) {
        this.text = text;
    }


    private List<Beitrag> beitragList = new ArrayList<Beitrag>();

}
