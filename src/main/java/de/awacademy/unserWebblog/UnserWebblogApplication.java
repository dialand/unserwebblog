package de.awacademy.unserWebblog;

import de.awacademy.unserWebblog.beitrag.BeitragController;
import de.awacademy.unserWebblog.kommentar.Kommentar;
import de.awacademy.unserWebblog.kommentar.KommentarController;
import de.awacademy.unserWebblog.user.User;
import de.awacademy.unserWebblog.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class UnserWebblogApplication {

    @Autowired
    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(UnserWebblogApplication.class, args);
    }

    @PostConstruct
    private void erstelleInitialenAdmin() {
        if (!userRepository.existsByName("admin") && !userRepository.existsByName("sabina") &&
                !userRepository.existsByName("leonardo") && !userRepository.existsByName("diana")
                && !userRepository.existsByName("cristina") && !userRepository.existsByName("ernie")) {

            User adminUserInitial = new User("admin", "admin", true);
            userRepository.save(adminUserInitial);
            User adminUserInitialSabina = new User("sabina", "sabina", true);
            userRepository.save(adminUserInitialSabina);
            User adminUserInitialLeo = new User("leonardo", "leonardo", true);
            userRepository.save(adminUserInitialLeo);
            User adminUserInitialDiana = new User("diana", "diana", true);
            userRepository.save(adminUserInitialDiana);
            User adminUserInitialCristina = new User("cristina", "cristina", false);
            userRepository.save(adminUserInitialCristina);
            User adminUserInitialErnie = new User("ernie", "bert", false);
            userRepository.save(adminUserInitialErnie);
        }
    }


}
