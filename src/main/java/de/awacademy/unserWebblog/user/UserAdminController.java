package de.awacademy.unserWebblog.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserAdminController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/showAdmins")
    public String zeigeAdminStatus(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "nameToAdmin";
    }

    @PostMapping("/nameToAdmin")
    public String ernennenAdmin(@ModelAttribute("currentUser") User currentUser, @RequestParam("userId") long userId) {
        User user = userRepository.findById(userId).get();
        if (currentUser.isAdministrator()) {
            if (!user.isAdministrator()) {
                user.setAdministrator(true);
                userRepository.saveAndFlush(user);
                return "redirect:/showAdmins";
            }
        }
        return "redirect:/showAdmins";
    }


}
